<?php

namespace App\Form;

use App\Entity\Hero;
use App\Entity\Power;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;

class HeroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('birthdate', DateTime::class, [
                'widget' => 'single_text'
            ])
            ->add('level')
            ->add('name')
             ->add('powers'//, EntityType::class, [
            //     'multiple' => true,
            //     'class' => Power::class
            // ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Hero::class,
            'csrf_protection' => false
        ]);
    }
}