<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 *  @Route("/api")
 */
class ApiAuthController extends AbstractController
{
    /**
     * @Route(methods="GET")
     */
    public function index(UserRepository $repo)
    {
       return $this->json($repo->findBy([]));
    }

    /**
     * @Route("/user/auth", methods="POST")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $manager) 
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user,  array(  'csrf_protection' => false
            
        ));
        $form->submit(
            //On utilise la méthode json_decode pour transformer le contenu de la
            //request (qui est une chaîne de caractère json) en tableau associatif
            json_decode(
                $request->getContent(),
                true
            ), 
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $hashedPassword = $encoder->encodePassword($user,$user->getPassword());
            $user->setRoles(['ROLE_USER'])
                ->setPassword($hashedPassword);
            
            $manager->persist($user);
            $manager->flush();
            return $this->json($user, Response::HTTP_CREATED);
        }
        return $this->json($form->getErrors(true), Response::HTTP_BAD_REQUEST);
    }
}