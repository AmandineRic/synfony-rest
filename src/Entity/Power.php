<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PowerRepository")
 */
class Power
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Hero", inversedBy="power")
     */
    private $HeroPower;

    public function __construct()
    {
        $this->HeroPower = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Hero[]
     */
    public function getHeroPower(): Collection
    {
        return $this->HeroPower;
    }

    public function addHeroPower(Hero $heroPower): self
    {
        if (!$this->HeroPower->contains($heroPower)) {
            $this->HeroPower[] = $heroPower;
        }

        return $this;
    }

    public function removeHeroPower(Hero $heroPower): self
    {
        if ($this->HeroPower->contains($heroPower)) {
            $this->HeroPower->removeElement($heroPower);
        }

        return $this;
    }
}
