<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HeroRepository")
 */
class Hero
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Power", mappedBy="HeroPower")
     */
    private $power;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Monster", mappedBy="heros", orphanRemoval=true)
     */
    private $monster;

    public function __construct()
    {
        $this->power = new ArrayCollection();
        $this->monster = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Power[]
     */
    public function getPower(): Collection
    {
        return $this->power;
    }

    public function addPower(Power $power): self
    {
        if (!$this->power->contains($power)) {
            $this->power[] = $power;
            $power->addHeroPower($this);
        }

        return $this;
    }

    public function removePower(Power $power): self
    {
        if ($this->power->contains($power)) {
            $this->power->removeElement($power);
            $power->removeHeroPower($this);
        }

        return $this;
    }

    /**
     * @return Collection|Monster[]
     */
    public function getMonster(): Collection
    {
        return $this->monster;
    }

    public function addMonster(Monster $monster): self
    {
        if (!$this->monster->contains($monster)) {
            $this->monster[] = $monster;
            $monster->setHeros($this);
        }

        return $this;
    }

    public function removeMonster(Monster $monster): self
    {
        if ($this->monster->contains($monster)) {
            $this->monster->removeElement($monster);
            // set the owning side to null (unless already changed)
            if ($monster->getHeros() === $this) {
                $monster->setHeros(null);
            }
        }

        return $this;
    }
}
