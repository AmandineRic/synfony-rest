<?php

namespace App\DataFixtures;

use App\Entity\Hero;
use App\Entity\Power;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {       
        $faker = Factory::create();
        
        $users = [];

        $user = new User();
        $hashedPassword = $this->encoder->encodePassword($user, '1234');
        
        $user->setEmail('user@user.com')
            ->setPassword($hashedPassword)
            ->setRoles(['ROLE_USER']);
        $manager->persist($user);
        $users[] = $user;

        
        for ($i=0; $i < 5; $i++) { 
            $power = new Power();
            $power->setName('power '.$i)->setType('type '.$i);
            $manager->persist($power);
            $powers[] = $power;
        }
        
        for ($i=0; $i < 5; $i++) { 
            $hero = new Hero();
            $hero->setName($faker->name)
                ->setLevel($faker->randomNumber())
                ->setBirthdate($faker->dateTime())
                ->setTitle('The Great ' . $faker->firstName);
            for ($y=0; $y < 2; $y++) { 
                 $hero->addPower($powers[random_int(0, 4)]);
            }
            $manager->persist($hero);
        }
        $manager->flush();
    }
}