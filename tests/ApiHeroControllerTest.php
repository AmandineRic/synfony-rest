<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

class ApiHeroControllerTest extends WebTestCase
{
    public function setUp(){
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();
    }
    
    public function testGetAll()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/hero');

        $data = json_decode($client->getResponse()->getContent(), true);
    
        $this->assertResponseIsSuccessful();
        
        $this->assertCount(5, $data);
        $this->assertTrue(is_int($data[0]['level']));
        
    }

    public function testAddHero(){
        $client = static::createClient();
        
        $json = json_encode(['name' => 'test', 'title' => 'title test', 'birthdate' => '2000-01-01', 'level' => '10']);
        
        $client->request('POST', '/api/hero', [], [], [], $json);
        $this->assertResponseStatusCodeSame(201);
    }
}